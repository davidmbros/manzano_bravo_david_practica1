package com.example.david.manzano_bravo_david_practica1;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by david on 08/12/2017.
 */

public class BaseDatos extends SQLiteOpenHelper {

    Context contexto;

    public BaseDatos(Context context) {
        super(context, "BD", null, 24);
        this.contexto=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            //Creamos la tabla
            db.execSQL("CREATE TABLE Preguntas" +
                    "(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "pregunta TEXT, respcorrecta TEXT, respincor1 TEXT, " +
                    "respincor2 TEXT, respincor3 TEXT)");

            db.execSQL("CREATE TABLE Puntos(_id INTEGER PRIMARY KEY AUTOINCREMENT, usuario TEXT, punto TEXT, fecha TEXT)");

            //db.execSQL("INSERT INTO Preguntas (pregunta,respcorrecta,respincor1,respincor2,respincor3) " +
            //        "values('','','','','')");
            db.execSQL("INSERT INTO Preguntas (pregunta,respcorrecta,respincor1,respincor2,respincor3) " +
                    "values('¿Qué país fundaron con su matrimonio los reyes católicos?','España','Portugal','Castilla','Francia')");
            db.execSQL("INSERT INTO Preguntas (pregunta,respcorrecta,respincor1,respincor2,respincor3) " +
                    "values('¿Cómo se llama la hija pequeña de los Simpsons?','Maggie','Lisa','Rachel','Marge')");
            db.execSQL("INSERT INTO Preguntas (pregunta,respcorrecta,respincor1,respincor2,respincor3) " +
                    "values('¿Quién es el pintor de la \"Mona Lisa\"?','Leonardo da Vinci','Edvard Munch','Salvador Dalí','Esteban')");
            db.execSQL("INSERT INTO Preguntas (pregunta,respcorrecta,respincor1,respincor2,respincor3) " +
                    "values('¿Cuál de estas novelas no fue escrita por Miguel de Cervantes?','La familia de Pascual Duarte'," +
                    "'Don Quijote de la Mancha','Los trabajos de Persiles y Segismunda','La gitanilla')");
            db.execSQL("INSERT INTO Preguntas (pregunta,respcorrecta,respincor1,respincor2,respincor3) " +
                    "values('¿Qué equipo gano el mundial de fútbol 2010 en Sudáfrica?','España','Países Bajos','Alemania','Uruguay')");
            db.execSQL("INSERT INTO Preguntas (pregunta,respcorrecta,respincor1,respincor2,respincor3) " +
                    "values('¿De qué país son las islas Canarias?','España','Argentina','Ecuador','Italia')");


        }
        catch (SQLException e){
            //Mensaje de error si no se ha ejecutado correctamente
            Toast.makeText(contexto,""+e,Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            //Eliminamos las tabla anterior (si existe)
            db.execSQL("DROP TABLE IF EXISTS Preguntas");

            db.execSQL("DROP TABLE IF EXISTS Puntos");
            //Llamamos a onCreate para que cree la tabla con las nuevas especificaciones
            onCreate(db);
        }
        catch (SQLException e){
            //Mensaje de error si no se ha ejecutado correctamente
            Toast.makeText(contexto,""+e,Toast.LENGTH_SHORT).show();
        }
    }
}
