package com.example.david.manzano_bravo_david_practica1;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

public class GameActivity extends AppCompatActivity {

    BDAdaptador bdAdaptador;
    TextView usuario, pregunta, puntos;
    Chronometer crono;
    CountDownTimer tiempojuego;

    int puntuacionfinal = 0;
    int PreguntaN = 1;
    Button respuesta1,respuesta2,respuesta3,respuesta4,siguiente;
    Cursor cursor;
    ArrayList<Integer> listaNumero = new ArrayList();
    String fecha="";
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_game);

        usuario = findViewById(R.id.tvUsuario);
        pregunta = findViewById(R.id.tvPregunta);
        puntos = findViewById(R.id.txtPuntos);
        crono = findViewById(R.id.chronometer2);
        respuesta1=findViewById(R.id.btnr1);
        respuesta2=findViewById(R.id.btnr2);
        respuesta3=findViewById(R.id.btnr3);
        respuesta4=findViewById(R.id.btnr4);
        siguiente=findViewById(R.id.btSiguiente);
        bdAdaptador = new BDAdaptador(GameActivity.this);


        SharedPreferences preferencias = getSharedPreferences("com.example.david.manzano_bravo_david_practica1_preferences", Context.MODE_PRIVATE);
        final String user=preferencias.getString("usuario","sin_user");
        String dificult= preferencias.getString("dificultad","1");

        usuario.setText(user);

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy / HH:mm");
        fecha = df.format(Calendar.getInstance().getTime());

        int tiempo = 30000;

        if(dificult.equals("1")){
            tiempo = 30000;
        }else {
            if(dificult.equals("2")){
                tiempo = 20000;
            }else {
                tiempo = 10000;
            }
        }

        tiempojuego = new CountDownTimer(tiempo, 1000) {

            public void onTick(long millisUntilFinished) {

                crono.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                crono.setText("Fin!");
                //Insertar puntos y usuario

                //Cuadro Dialogo
                AlertDialog.Builder builder= new AlertDialog.Builder(GameActivity.this);
                builder.setCancelable(false);
                builder.setTitle("Atención");
                builder.setMessage("¿Quieres Jugar otra partida?");

                builder.setPositiveButton("Jugar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        bdAdaptador.guardarPuntuacion(user,String.valueOf(puntuacionfinal),fecha);
                        Intent intent = new Intent(GameActivity.this, GameActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });
                builder.setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        bdAdaptador.guardarPuntuacion(user,String.valueOf(puntuacionfinal),fecha);
                        Intent intent = new Intent(GameActivity.this,MenuActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });
                builder.create();
                builder.show();
            }
        }.start();


        //Lanzamos la pregunta;
            nuevaPregunta();
        respuesta2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarPregunta(respuesta2);
            }
        });

        respuesta3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarPregunta(respuesta3);
            }
        });

        respuesta4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarPregunta(respuesta4);
            }
        });

        siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nuevaPregunta();
                respuesta1.setEnabled(true);
                respuesta2.setEnabled(true);
                respuesta3.setEnabled(true);
                respuesta4.setEnabled(true);

                respuesta1.setTextColor(Color.parseColor("#ffffff"));
                respuesta2.setTextColor(Color.parseColor("#ffffff"));
                respuesta3.setTextColor(Color.parseColor("#ffffff"));
                respuesta4.setTextColor(Color.parseColor("#ffffff"));
                siguiente.setVisibility(View.INVISIBLE);
            }
        });



    }

    public void onBackPressed() {

        tiempojuego.cancel();

        AlertDialog.Builder builder= new AlertDialog.Builder(GameActivity.this);
        builder.setCancelable(false);
        builder.setTitle("Atención");
        builder.setMessage("¿Estás seguro de que quieres salir al menu principal?");

        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
                Intent intent = new Intent(GameActivity.this,MenuActivity.class);
                //tiempojuego.cancel();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tiempojuego.start();
            }
        });
        builder.create();
        builder.show();
    }

    public  void nuevaPregunta(){
        listaNumero = new ArrayList<>();
        if(PreguntaN<=6){
            Random rn = new Random();

            cursor = bdAdaptador.consultaPregunta(PreguntaN);
            cursor.moveToFirst();
            String pr = cursor.getString(1).toString();
            pregunta.setText(pr);

            //Genera numeros aleatorios para añadir luego las respuestas del cursor a los botones
            do{
                generar();
            }while (generar()!=-1);

            respuesta1.setText(cursor.getString(listaNumero.get(0)).toString());
            respuesta2.setText(cursor.getString(listaNumero.get(1)).toString());
            respuesta3.setText(cursor.getString(listaNumero.get(2)).toString());
            respuesta4.setText(cursor.getString(listaNumero.get(3)).toString());

            respuesta1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    comprobarPregunta(respuesta1);
                }
            });
        }else{
            tiempojuego.cancel();
            AlertDialog.Builder builder= new AlertDialog.Builder(GameActivity.this);
            builder.setTitle("Atención");
            builder.setMessage("PARTIDA TERMINADA, ¿Quieres Jugar otra partida?");

            builder.setPositiveButton("Jugar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    bdAdaptador.guardarPuntuacion(usuario.getText().toString(),String.valueOf(puntuacionfinal),fecha);
                    Intent intent = new Intent(GameActivity.this, GameActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
            builder.setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    bdAdaptador.guardarPuntuacion(usuario.getText().toString(),String.valueOf(puntuacionfinal),fecha);
                    Intent intent = new Intent(GameActivity.this,MenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
            builder.create();
            builder.show();
        }

    }

    public int aleatorio(){
        int n= (int) (Math.random()*(5-2+1)+2);;
        return n;
    }

    public int generar(){
        //Generar numeros aleatorios y los añade al arraylist listanumeros
        // para distribuir las respuestas aleatoriamente.
        //Los numeros aleatorios son entre 2 y 5 que son las posiciones del las respuestas
        //en el cursor para añadirselas despues a los botones
        if(listaNumero.size() < (5 - 2) +1){
            int numero = aleatorio();
            if(listaNumero.isEmpty()){
                listaNumero.add(numero);
                return numero;
            }else{
                if(listaNumero.contains(numero)){
                    return generar();
                }else{
                    listaNumero.add(numero);
                    return numero;
                }
            }
        }else{
            return -1;
        }
    }

    //Se le pasa el boton pulsado y comprueba si la respuesta es la correcta con la del cursor
    //En el cursor la respuesta correcta siempre esta en el 2

    public void comprobarPregunta(Button rselecionada){

        if(rselecionada.getText().toString().equals(cursor.getString(2).toString())){
            rselecionada.setTextColor(Color.parseColor("#2ECC71"));
            puntuacionfinal+=10;
            puntos.setText(String.valueOf(puntuacionfinal));
            PreguntaN++;

            siguiente.setVisibility(View.VISIBLE);
            respuesta1.setEnabled(false);
            respuesta2.setEnabled(false);
            respuesta3.setEnabled(false);
            respuesta4.setEnabled(false);

        }else{
            rselecionada.setTextColor(Color.parseColor("#B71C1C"));
            PreguntaN++;

            siguiente.setVisibility(View.VISIBLE);
            respuesta1.setEnabled(false);
            respuesta2.setEnabled(false);
            respuesta3.setEnabled(false);
            respuesta4.setEnabled(false);
        }


    }

}
