package com.example.david.manzano_bravo_david_practica1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AyudaActivity extends AppCompatActivity {

    Button volver;
    TextView info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_ayuda);


        info = findViewById(R.id.txtInfo);
        volver=findViewById(R.id.btnVolver);

        info.setText("Este juego consiste en ir respondiendo* una serie de preguntas de caracter general, " +
                "si respondes bien a la pregunta se suma 10 puntos y si la pregunta es erronea no suma nada de puntos." +
                "\n\nLa dificultad del juego:" +
                "\n\nCerebro de pez: 30 segundos" +
                "\nEn la media: 20 segundos" +
                "\nNeurona Fast: 10 segundos" +
                "\n\n*Todas las respuestas son aleatorias y no se situan siempre en el mismo botón." +
                "\n\n¿PREPARADO PARA EL RETO?");

        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AyudaActivity.this,MenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

    }

    public void onBackPressed() {
        // Write your code here
        Intent intent = new Intent(AyudaActivity.this,MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        super.onBackPressed();
    }
}
