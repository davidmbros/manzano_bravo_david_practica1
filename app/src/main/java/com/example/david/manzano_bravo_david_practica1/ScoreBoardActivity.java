package com.example.david.manzano_bravo_david_practica1;

import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ScoreBoardActivity extends AppCompatActivity {

    BDAdaptador bd;
    TextView titulo;
    Cursor c1;
    ListView listaPuntos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_scoreboard);

        listaPuntos = findViewById(R.id.lvPuntuaciones);
        titulo = findViewById(R.id.txtTitulo);
        bd = new BDAdaptador(ScoreBoardActivity.this);
/*
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        Cursor c = bd.mostrarPuntuacion();
            while (c.moveToNext()){
                adapter.add("Usuario: "+c.getString(1)+"\nPuntos: "+c.getString(2)+"\nFecha: "+c.getString(3));

            }
        listaPuntos.setAdapter(adapter);

*/

        c1 = bd.mostrarPuntuacion();
        String[] from = new String[]{"usuario","punto"};
        int[] to = new int[]{R.id.tvnombreScore,R.id.tvPuntosScore};
        final SimpleCursorAdapter cursorAdapter =
               new SimpleCursorAdapter(ScoreBoardActivity.this,R.layout.customscorelist,c1,from,to);
        listaPuntos.setAdapter(cursorAdapter);



        listaPuntos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                c1.moveToPosition(i);
                String usuario = c1.getString(1);
                String puntos = c1.getString(2);
                String fecha = c1.getString(3);
                Toast.makeText(ScoreBoardActivity.this,"Usuario: "+usuario+" Puntos: "+puntos+
                        "\nFecha: "+fecha,Toast.LENGTH_SHORT).show();
            }
        });

        listaPuntos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final int posicion=i;

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ScoreBoardActivity.this);
                alertDialog.setTitle("Borrar");
                alertDialog.setMessage("Borrar esta puntuación?");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        c1.moveToPosition(posicion);
                        int id = c1.getInt(0);
                        bd.eliminar(id);
                        c1=bd.mostrarPuntuacion();
                        cursorAdapter.swapCursor(c1);
                    }
                }) ;
                alertDialog.setNegativeButton("Cancelar",null);
                alertDialog.create();
                alertDialog.show();

                return true;
            }
        });


    }
}
