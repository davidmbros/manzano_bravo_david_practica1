package com.example.david.manzano_bravo_david_practica1;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.PreparedStatement;


/**
 * Created by david on 08/12/2017.
 */

public class BDAdaptador {

    private Context contexto;
    private BaseDatos baseDatos;
    private SQLiteDatabase bd;
    private SQLiteDatabase bd2;

    public BDAdaptador(Context c) {
        this.contexto = c;
        baseDatos=new BaseDatos(c);
        this.bd = baseDatos.getWritableDatabase();
        this.bd2=baseDatos.getReadableDatabase();
    }


    public Cursor consultaPregunta(int id){

        Cursor cursor = bd2.rawQuery("SELECT * FROM Preguntas WHERE _id="+id,null);

        return cursor;
    }

    public void guardarPuntuacion(String user,String punto,String fecha){

        bd.execSQL("INSERT INTO Puntos(usuario,punto,fecha) VALUES ('"+user+"','"+punto+"','"+fecha+"')");

    }

    public Cursor mostrarPuntuacion(){

        Cursor cursor= bd2.rawQuery("SELECT * FROM Puntos",null);

        return cursor;
    }

    public void eliminar(int id){
        bd2.execSQL("delete FROM Puntos where _id=" + id);
    }
}
