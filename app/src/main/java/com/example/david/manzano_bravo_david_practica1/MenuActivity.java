package com.example.david.manzano_bravo_david_practica1;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity {

    Button jugar,marcadores,salir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_menu);

        jugar=findViewById(R.id.btnJugar);
        marcadores=findViewById(R.id.btnMarcadores);
        salir=findViewById(R.id.btnSalir);

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder= new AlertDialog.Builder(MenuActivity.this);
                builder.setCancelable(false);
                builder.setTitle("Atención");
                builder.setMessage("¿Estás seguro de que quieres salir?");

                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                builder.setNegativeButton("No",null);
                builder.create();
                builder.show();

            }
        });

        jugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, GameActivity.class);
                startActivity(intent);
            }
        });

        marcadores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this,ScoreBoardActivity.class);
                startActivity(intent);
            }
        });

    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuconfig, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.mnConfig:
                Intent intentconf = new Intent(MenuActivity.this,ConfigActivity.class);
                startActivity(intentconf);
                return true;
            case R.id.mnAyuda:
                Intent intent = new Intent(MenuActivity.this,AyudaActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


        public void onBackPressed() {

            AlertDialog.Builder builder= new AlertDialog.Builder(MenuActivity.this);
            builder.setCancelable(false);
            builder.setTitle("Atención");
            builder.setMessage("¿Estás seguro de que quieres salir?");

            builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            builder.setNegativeButton("No",null);
            builder.create();
            builder.show();
        }


    }


